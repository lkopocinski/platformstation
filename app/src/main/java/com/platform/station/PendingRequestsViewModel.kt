package com.platform.station

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PendingRequestsViewModel(
        private val requestsRepository: RequestsRepository = RequestsRepository()
) : ViewModel() {

    fun getRequestsLiveData(): LiveData<List<Request>> = requestsLiveData
    private val requestsLiveData = MutableLiveData<List<Request>>()

    fun init() {
        requestsRepository.listenAllRequests {
            displayRequests(it)
        }
    }

    private fun displayRequests(requests: List<Request>) {
        val sortedRequest = requests.sortedBy { it.arrivalTime }
        requestsLiveData.postValue(sortedRequest)
    }

    fun finishRequest(id: String?) {
        requestsRepository.finishRequest(id)
    }
}