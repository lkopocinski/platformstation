package com.platform.station

data class Request(
        var id: String?,
        val name: String,
        val surname: String,
        val arrivalTime: String,
        val finalDestination: String,
        val rampRequired: Boolean
)