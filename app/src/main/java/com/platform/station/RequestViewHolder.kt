package com.platform.station

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.request_row.*

class RequestViewHolder(
        override val containerView: View,
        private val onRequestClicked: RequestItemClickListener
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(request: Request) {
        requestItem.setOnClickListener { onRequestClicked.invoke(request) }
        requesterNameAndSurname.text = request.name + request.surname
        arrivalTime.text = request.arrivalTime
        finalDestination.text = request.finalDestination
        rampRequired.visibility = if (request.rampRequired) View.VISIBLE else View.INVISIBLE
    }
}