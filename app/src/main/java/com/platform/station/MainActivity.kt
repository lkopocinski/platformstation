package com.platform.station

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var requestAdapter: RequestsAdapter

    private val viewModel: PendingRequestsViewModel = PendingRequestsViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        subscribeRequests()

        viewModel.init()
    }

    private fun subscribeRequests() {
        viewModel.getRequestsLiveData().observe(this, Observer {
            requestAdapter.submitList(it)
        })
    }

    private fun setupRecyclerView() {
        requestAdapter = RequestsAdapter {
            viewModel.finishRequest(it.id)
        }

        recyclerView.adapter = requestAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}

