package com.platform.station

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.moshi.Moshi


const val NODE = "pendingRequest"

class RequestsRepository {

    val database: FirebaseDatabase by lazy { FirebaseDatabase.getInstance() }

    fun listenAllRequests(onRequestReceived: (List<Request>) -> Unit) {
        val reference = database.getReference(NODE)

        reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val requests: List<Request> = dataSnapshot.children.mapNotNull {
                    serializeRequest(it.key!!, it.value as String)
                }

                onRequestReceived(requests)
            }
        })
    }

    fun finishRequest(id: String?) {
        val reference = database.getReference(NODE).child(id ?: "")

        reference.removeValue()
    }

    private fun serializeRequest(key: String, json: String): Request? {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter(Request::class.java)

        return jsonAdapter.fromJson(json)?.apply { id = key }
    }
}