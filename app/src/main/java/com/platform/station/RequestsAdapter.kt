package com.platform.station

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter


typealias RequestItemClickListener = (Request) -> Unit


private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Request>() {
    override fun areItemsTheSame(oldRequest: Request, newRequest: Request): Boolean {
        return oldRequest == newRequest
    }

    override fun areContentsTheSame(oldRequest: Request, newRequest: Request): Boolean {
        return oldRequest == newRequest
    }
}

class RequestsAdapter constructor(
        private val onRequestClicked: RequestItemClickListener
) : ListAdapter<Request, RequestViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestViewHolder {
        return RequestViewHolder(parent.inflateChild(R.layout.request_row, false), onRequestClicked)
    }

    override fun onBindViewHolder(holder: RequestViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}